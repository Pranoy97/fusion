#!/usr/bin/env python
import sys
import cv2
import time
import numpy as np
import os.path

from helper_functions import variance_of_laplacian
from helper_functions import cropped_to_original_frame
from helper_functions import norm_cross_correlation
from helper_functions import YOLO_on_ROI
from helper_functions import multithreading
from helper_functions import multiprocessing
from helper_functions import NCC
from color_classification import HistogramColorClassifier

from constants_and_global_vars import threshold
from constants_and_global_vars import prob_thresh
from constants_and_global_vars import prob_thresh_for_templates
from constants_and_global_vars import variance_thresh	

sys.path.append('../')
sys.path.append('../yolo_python_wrapper')
from darknet_customized import YOLO_v2
from darknet_customized import YOLO_v2_multi	
from darknet_customized import YOLO_v2_spl_fun	
from tracker import re3_tracker

class fusionTracker(object):
	def __init__(self,starting_bbox,debug):
		self.tracker = re3_tracker.Re3Tracker()
		self.num_frames = 0
		self.fps_total = 0
		self.counter = 0
		self.bbox = []
		self.BBox_from_yolo = []
		self.start = True
		self.prob = 0
		self.img_for_yolo = np.zeros((640,480,3))
		self.num_of_models = 0
		self.max_prob = 0
		self.lost_track = False
		self.image = np.zeros((640,480,3))		
		self.starting_bbox = starting_bbox
		self.debug = debug
		self.initial_bboxes = {} # for multitracker method
		self.bboxes = []         # for multiTracker method
		self.counter_for_obj = 0 # for multiTracker method
		self.known = True
		self.start_yolo = True
		self.cls = ''
		self.classes = []       # for multitracking method
		self.templates = []
		self.color_classifier = HistogramColorClassifier(channels=[0, 1, 2], 
                                         hist_size=[128, 128, 128], 
                                         hist_range=[0, 256, 0, 256, 0, 256], 
                                         hist_type='BGR')

	def track(self,image):
		self.image = image
		begin_time = time.time()
		
		#tracking begins here,i.e start of tracking getting the bounding box from the user 
		if self.start:
			self.BBox_from_yolo = self.starting_bbox
			#initializing re3
			imageRGB = self.image[:,:,::-1]
			self.bbox = self.tracker.track('ardrone', imageRGB,self.starting_bbox)

			#check if YOLO knows this object or not
			w = self.starting_bbox[2]-self.starting_bbox[0]
			h = self.starting_bbox[3]-self.starting_bbox[1]
			x1 = int(self.starting_bbox[0]-1*w)
			x2 = int(self.starting_bbox[2]+1*w)
			y1 = int(self.starting_bbox[1]-1*h)
			y2 = int(self.starting_bbox[3]+1*h)	
			if y1<0: y1 = 0
			if x1<0: x1 = 0
			if y2>=self.image.shape[0]: y2 = self.image.shape[0]-1
			if x2>=self.image.shape[1]: x2 = self.image.shape[1]-1		
			self.img_for_yolo = self.image[y1:y2,x1:x2]
			self.prob,pt1,pt2,self.cls = YOLO_v2(self.img_for_yolo)		
			self.known = (not (self.prob==0))

			#updating counters
			self.counter+=1
			self.start = False

			#saving the template
			x1 = int(self.bbox[0])
			x2 = int(self.bbox[2])
			y1 = int(self.bbox[1])
			y2 = int(self.bbox[3])	
			template = cv2.cvtColor(self.image[y1:y2,x1:x2], cv2.COLOR_BGR2GRAY)
			self.templates.append(template)
			self.num_of_models+=1
			self.color_classifier.addModelHistogram(self.image[y1:y2,x1:x2],name=str(self.num_of_models))
			if self.known: print('I am tracking a '+self.cls)
			else: print("I am tracking an object that I haven't seen before")

			return self.bbox		


		if self.known: 

			if not self.lost_track:
				
				if (self.counter>=threshold or (variance_of_laplacian(self.img_for_yolo)<variance_thresh)):
					
					w = self.bbox[2]-self.bbox[0]
					h = self.bbox[3]-self.bbox[1]
					x1 = int(self.bbox[0]-1*w)
					x2 = int(self.bbox[2]+1*w)
					y1 = int(self.bbox[1]-1*h)
					y2 = int(self.bbox[3]+1*h)
					# condition checking
					if y1<0: y1 = 0
					if x1<0: x1 = 0
					if y2>=self.image.shape[0]: y2 = self.image.shape[0]-1
					if x2>=self.image.shape[1]: x2 = self.image.shape[1]-1

					
					self.img_for_yolo = self.image[y1:y2,x1:x2]
					
					if self.img_for_yolo.shape[0]>0 and self.img_for_yolo.shape[1]>0:
						self.prob,pt1,pt2 = YOLO_v2_spl_fun(self.img_for_yolo,self.cls)
						bbox_now = [pt1[0],pt1[1],pt2[0],pt2[1]]
						self.BBox_from_yolo = cropped_to_original_frame(self.bbox,bbox_now,self.image.shape)

					# updating the re3 tracker
					if self.prob>prob_thresh and (self.img_for_yolo.shape[0]>0 and self.img_for_yolo.shape[1]>0) and self.counter==threshold: # if prob is good then update the bounding box 
						initial_bbox = self.BBox_from_yolo
						self.counter = 0
						imageRGB = self.image[:,:,::-1]
						self.bbox = self.tracker.track('ardrone', imageRGB)#,initial_bbox)
					else:
						print('lost track',self.prob,self.counter)
						self.lost_track = True
						self.counter = 0

				else:	
					imageRGB = self.image[:,:,::-1]
					self.bbox = self.tracker.track('ardrone', imageRGB)	
					self.counter+=1					

			if self.lost_track:
				# run yolo on the entire image as object not found in the expected ROI

				bboxes_from_yolo = YOLO_v2_multi(self.image,self.cls)

				if len(bboxes_from_yolo)==0:print('object not found')
				
				list_for_keeping_track_of_bbox_match = []

				#if sufficient templates are present then follow the below paradigm to do a sort of low-end recognition of object
				if self.num_of_models>=10 and bboxes_from_yolo!=None:
					best_score = -1.0
					for item in bboxes_from_yolo:
						self.prob = item[0]
						if self.prob>prob_thresh:
							bbox_ = [item[1][0],item[1][1],item[2][0],item[2][1]]
							x1 = int(bbox_[0])
							x2 = int(bbox_[2])
							y1 = int(bbox_[1])
							y2 = int(bbox_[3])
							comparison_array = self.color_classifier.returnHistogramComparisonArray(self.image[y1:y2,x1:x2],method="intersection") 		
							color_hist_score = np.max(comparison_array)
							NCC_score = NCC(self.templates,self.image[y1:y2,x1:x2])
							score = color_hist_score+NCC_score
							#print(color_hist_score,NCC_score)
							if score>best_score and NCC_score>0.7 and color_hist_score>0.7:	
								best_score = score
								best_bbox = bbox_		

					#print best_score
					if best_score>1.4:	
						self.lost_track = False			
						self.counter = 0
						imageRGB = self.image[:,:,::-1]
						initial_bbox = best_bbox
						self.bbox = self.tracker.track('ardrone', imageRGB,initial_bbox)					

					else:# let the re3 tracker keep tracking for now			
						imageRGB = self.image[:,:,::-1]
						try:
							self.bbox = self.tracker.track('ardrone', imageRGB)
						except:
							pass
				else:			
					imageRGB = self.image[:,:,::-1]
					try:
						self.bbox = self.tracker.track('ardrone', imageRGB)
					except:
						pass

		# object never seen by YOLO	
		else:
			imageRGB = self.image[:,:,::-1]
			self.bbox = self.tracker.track('ardrone', imageRGB)
			self.counter+=1																							

		# saving templates(i.e. first 30 bbox)
		if self.num_of_models<30 and self.prob>prob_thresh_for_templates:
			x1 = int(self.bbox[0])
			x2 = int(self.bbox[2])
			y1 = int(self.bbox[1])
			y2 = int(self.bbox[3])	
			self.num_of_models+=1
			self.color_classifier.addModelHistogram(self.image[y1:y2,x1:x2],name=str(self.num_of_models))
			template = cv2.cvtColor(self.image[y1:y2,x1:x2], cv2.COLOR_BGR2GRAY)
			self.templates.append(template)				
						
		end_time = time.time()
		self.num_frames+=1
		self.fps_total+= 1/(end_time-begin_time)
		avg_fps = self.fps_total/self.num_frames
		return self.bbox

	def multi_track(self,image):
		self.image = image
		begin_time = time.time()

		#tracking begins here,i.e start of tracking getting the bounding box from the user 
		if self.start:
			print('multi tracking started')
			self.BBox_from_yolo = self.starting_bbox # here the starting_bbox is a list of lists(as multiple bbox are present)
			self.counter_for_obj = 0
			for item in self.BBox_from_yolo:
				self.counter_for_obj+=1
				bbox = [item[0],item[1],item[2],item[3]]
				self.initial_bboxes[self.counter_for_obj] = bbox
			#self.counter_for_obj = self.counter_for_obj_local	#updating the number of objects being tracked
			imageRGB = self.image[:,:,::-1]
			if self.counter_for_obj<2 and self.counter_for_obj>0:
				print("multiple objects not found")
				print('re-initializing multi tracking ....')
				self.start = True	
			elif self.counter_for_obj>=2:
				self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB,self.initial_bboxes) 	
				self.counter+=1
				self.start = False
			else:
				self.start = True # as object not found yet
				print("no objects found in the current frame at the begining")

		# if counter is greater than its threshold or if the image is blurry i.e. variance of laplacian is less than its thresh, then use YOLO(if possible) 
		elif self.counter>=threshold or (variance_of_laplacian(self.image)<variance_thresh):
			start = time.time()
			args = []
			for bbox in self.bboxes:
				args.append([self.image,bbox])										
			res = multithreading(YOLO_on_ROI, args, 1)

			print(time.time()-start)
			# updating the re3 tracker
			self.counter_for_obj = 0
			self.initial_bboxes = {}
			print('after multithreading')
			for item in res:
				self.counter_for_obj+=1
				prob = item[0]
				if prob>prob_thresh:
					self.initial_bboxes[self.counter_for_obj] = item[1]
			imageRGB = self.image[:,:,::-1]
			# following pythonic EAFP(Easier to ask for forgiveness than permission)	
			try:
				self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB,self.initial_bboxes)
			except:
				print('multiple objects not found')
				print('re-initializing multi tracking ....')
				self.start = True		
			if len(self.bboxes)>=2:
				self.counter = 0	

		# just tracking by re3			
		else:
			#if self.debug:
			print('running re3')
			imageRGB = self.image[:,:,::-1]
			self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB)
			self.counter+=1	
						
		end_time = time.time()
		self.num_frames+=1
		self.fps_total+= 1/(end_time-begin_time)
		avg_fps = self.fps_total/self.num_frames
		if not self.debug:	
			print('Current fusion fps: %.3f FPS' % (1 / (end_time - begin_time)))
			print('average fusion fps: '+str(avg_fps)+' FPS')
			print('\n')
		return self.bboxes

	def multi_track_without_multithreading(self,image):
		self.image = image
		begin_time = time.time()

		#tracking begins here,i.e start of tracking getting the bounding box from the user 
		if self.start:
			print('multi tracking started')
			self.BBox_from_yolo = self.starting_bbox # here the starting_bbox is a list of lists(as multiple bbox are present)
			self.counter_for_obj = 0
			for item in self.BBox_from_yolo:
				self.counter_for_obj+=1
				bbox = [item[0],item[1],item[2],item[3]]
				#check if YOLO knows this object or not
				w = bbox[2]-bbox[0]
				h = bbox[3]-bbox[1]
				x1 = int(bbox[0]-1*w)
				x2 = int(bbox[2]+1*w)
				y1 = int(bbox[1]-1*h)
				y2 = int(bbox[3]+1*h)	
				if y1<0: y1 = 0
				if x1<0: x1 = 0
				if y2>=self.image.shape[0]: y2 = self.image.shape[0]-1
				if x2>=self.image.shape[1]: x2 = self.image.shape[1]-1		
				self.img_for_yolo = self.image[y1:y2,x1:x2]
				#print(YOLO_v2(self.img_for_yolo)[-1])
				self.classes.append(YOLO_v2(self.img_for_yolo)[-1])					
				self.initial_bboxes[self.counter_for_obj] = bbox
			print(self.classes)	
			#self.counter_for_obj = self.counter_for_obj_local	#updating the number of objects being tracked
			imageRGB = self.image[:,:,::-1]
			if self.counter_for_obj<2 and self.counter_for_obj>0:
				print("multiple objects not found")
				print('re-initializing multi tracking ....')
				self.start = True	
			elif self.counter_for_obj>=2:
				self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB,self.initial_bboxes) 	
				self.counter+=1
				self.start = False
			else:
				self.start = True # as object not found yet
				print("no objects found in the current frame at the begining")

		# if counter is greater than its threshold or if the image is blurry i.e. variance of laplacian is less than its thresh, then use YOLO(if possible) 
		elif self.counter>=threshold or (variance_of_laplacian(self.image)<variance_thresh):
			#print('yolo')
			start = time.time()
			bbox_list = []
			counter = 0
			for bbox in self.bboxes:
				w = bbox[2]-bbox[0]
				h = bbox[3]-bbox[1]
				x1 = int(bbox[0]-w/2)
				x2 = int(bbox[2]+w/2)
				y1 = int(bbox[1]-h/2)
				y2 = int(bbox[3]+h/2)
				# condition checking
				if y1<0: y1 = 0
				if x1<0: x1 = 0
				if y2>=image.shape[0]: y2 = image.shape[0]-1
				if x2>=image.shape[1]: x2 = image.shape[1]-1
				
				img_for_yolo = image[y1:y2,x1:x2]
				if img_for_yolo.shape[0]>0 and img_for_yolo.shape[1]>0:
					prob,pt1,pt2,cls = YOLO_v2(img_for_yolo)
					bbox_now = [pt1[0],pt1[1],pt2[0],pt2[1]]
					BBox_from_yolo = cropped_to_original_frame(bbox,bbox_now,image.shape)
					if cls == self.classes[counter]:
						bbox_list.append([prob,BBox_from_yolo])
					else:
						print('error')
						pass
							
				counter+=1							


			# updating the re3 tracker
			self.counter_for_obj = 0
			self.initial_bboxes = {}
			for item in bbox_list:
				self.counter_for_obj+=1
				prob = item[0]
				if prob>prob_thresh:
					self.initial_bboxes[self.counter_for_obj] = item[1]
			imageRGB = self.image[:,:,::-1]
			# following pythonic EAFP(Easier to ask for forgiveness than permission)	
			try:
				self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB)#,self.initial_bboxes)
			except:
				print('multiple objects not found')
				print('re-initializing multi tracking ....')
				self.start = True
				self.classes = []		
			if len(self.bboxes)>=2:
				self.counter = 0	

		# just tracking by re3			
		else:
			#if self.debug:
			#print('running re3')
			imageRGB = self.image[:,:,::-1]
			self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB)
			self.counter+=1	
						
		end_time = time.time()
		self.num_frames+=1
		self.fps_total+= 1/(end_time-begin_time)
		avg_fps = self.fps_total/self.num_frames
			 
		print('Current fusion fps: %.3f FPS' % (1 / (end_time - begin_time)))
		print('average fusion fps: '+str(avg_fps)+' FPS')
		print('\n')
		return self.bboxes		
