import numpy as np
import cv2

###global vars
num_frames = 0
fps_total = 0
counter = 0
bbox = []
BBox_from_yolo = []

start = True
prob = 0

img_for_yolo = np.zeros((640,480,3))

templates = []
max_prob = 0
lost_track = False
# global vars for cropping the desired region of the image
refPt = []
cropping = False

###global objects

# Initiate ORB detector
orb = cv2.ORB_create()


#important constants(that can potentially be changed)
# for TB-50 dataset(threshold is set low because almost all the videos have the objects as humans on which the yolo is already trained.
# threshold = 14
# prob_thresh = 0.55
# prob_thresh_for_templates = 0.7
# variance_thresh = 70

# for VOT17 dataset
threshold = 20
prob_thresh = 0.55
prob_thresh_for_templates = 0.7
variance_thresh = 70
