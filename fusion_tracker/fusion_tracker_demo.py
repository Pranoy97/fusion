import sys
# Uncomment the below given line if you are using ROS-kinetic
# sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages') 
import cv2
import time
import numpy as np

from fusion_tracker import fusionTracker
from helper_functions import get_the_desired_roi
from helper_functions import click_and_crop


def main():
	cam = cv2.VideoCapture(str(sys.argv[1]))
	ret,image = cam.read()
	counter = 0
	while ret:
		counter+=1
		if counter==1: #initialize fusion tracker
			starting_bbox = get_the_desired_roi(image)
			tracker_object = fusionTracker(starting_bbox,0)
			bbox = tracker_object.track(image)
		else:	
			bbox = tracker_object.track(image)
                        
		cv2.rectangle(image,(int(bbox[0]), int(bbox[1])),(int(bbox[2]), int(bbox[3])),[0,0,255], 2)
		cv2.imshow('fusion', image)
		if cv2.waitKey(10) & 0xFF == ord('q'):
			break
		ret,image = cam.read()
		
if __name__ == '__main__':
	main()
