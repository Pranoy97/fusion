import sys
import cv2
import time
import numpy as np
sys.path.append('../yolo_python_wrapper')
from darknet_customized import YOLO_v2_multi
from fusion_tracker import fusionTracker
from helper_functions import get_the_desired_roi
from helper_functions import click_and_crop


if __name__ == '__main__':

	fourcc = cv2.VideoWriter_fourcc(*'XVID')

	cam = cv2.VideoCapture(0) # read video stream from webcam
	begin = time.time()
	ret,image = cam.read()	
	
	out = cv2.VideoWriter('output.avi',fourcc, 30.0, (image.shape[1],image.shape[0]))
	BBoxes_from_yolo = []
	starting_bbox = get_the_desired_roi(image)
	BBoxes_from_yolo.append(starting_bbox)
	starting_bbox = get_the_desired_roi(image)
	BBoxes_from_yolo.append(starting_bbox)	

	print len(BBoxes_from_yolo)
	starting_bboxes = []
				
	tracker_object = fusionTracker(BBoxes_from_yolo,1)
	color = [[255,0,0],[0,255,0],[0,0,255],[0,255,255]]
	while ret:
		bboxes = tracker_object.multi_track_without_multithreading(image)
		try:
			#drawing the bounding box on the image and showing it
			counter = 0
			for bbox in bboxes:
				
				cv2.rectangle(image,
				        (int(bbox[0]), int(bbox[1])),
				        (int(bbox[2]), int(bbox[3])),
				        color[counter], 2)
				counter+=1
				
		except:
			print('object not found')
		cv2.imshow('fusion', image)
		out.write(image)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			#out.release()
			break
		ret,image = cam.read()				
