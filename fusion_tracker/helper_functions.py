import numpy as np
import cv2
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import time
import sys
sys.path.append('../yolo_python_wrapper')
from darknet_customized import YOLO_v2

#Helper functions

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not 
def click_and_crop(event, x, y, flags, param):
	# grab references to the global variables
	image = param
	global refPt,cropping
 
	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt = [(x, y)]
		cropping = True
 
	# check to see if the left mouse button was released
	elif event == cv2.EVENT_LBUTTONUP:
		# record the ending (x, y) coordinates and indicate that
		# the cropping operation is finished
		refPt.append((x, y))
		cropping = False
 
		# draw a rectangle around the region of interest
		cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
		cv2.imshow("image", image)

def get_the_desired_roi(image):
	# load the image, clone it, and setup the mouse callback function
	clone = image.copy()
	cv2.namedWindow("image for crop")
	cv2.setMouseCallback("image for crop", click_and_crop,param = image)
	 
	# keep looping until the 'q' key is pressed
	while True:
		# display the image and wait for a keypress
		cv2.imshow("image for crop", image)
		key = cv2.waitKey(1) & 0xFF
	 
		# if the 'r' key is pressed, reset the cropping region
		if key == ord("r"):
			image = clone.copy()
	 
		# if the 'c' key is pressed, break from the loop
		elif key == ord("c"):
			break
	 
	# if there are two reference points, then crop the region of interest
	# from teh image and display it
	if len(refPt) == 2:
		roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
		return [refPt[0][0],refPt[0][1],refPt[1][0],refPt[1][1]]

def variance_of_laplacian(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	if image.shape[0]>0 and image.shape[1]>0: 
		return cv2.Laplacian(image, cv2.CV_64F).var()
	else:
		return 10000		
def cropped_to_original_frame(bbox_prev,bbox_now,im_shape):
	xmin = bbox_prev[0]
	ymin = bbox_prev[1]
	xmax = bbox_prev[2]
	ymax = bbox_prev[3]
	w = xmax - xmin
	h = ymax - ymin
	if int((xmin - w/2))>0:
		x_min_new = bbox_now[0] + int((xmin - w/2))
	else:
		x_min_new = bbox_now[0]
	if int((ymin - h/2))>0:
		y_min_new = bbox_now[1]	+ int((ymin - h/2))
	else:
		y_min_new = bbox_now[1]				
	if int((xmax - w/2))>0:
		x_max_new = bbox_now[2] + int((xmin - w/2))
	else:
		x_max_new = bbox_now[2]
	if int((ymax - h/2))>0:
		y_max_new = bbox_now[3]+ int((ymin - h/2))
	else:
		y_max_new = bbox_now[3]	

	if x_min_new<0: x_min_new = 0
	if y_min_new<0: y_min_new = 0
	if x_max_new>=im_shape[1]: x_max_new = im_shape[1]-1
	if y_max_new>=im_shape[0]: y_max_new = im_shape[0]-1

	return [x_min_new,y_min_new,x_max_new,y_max_new]
def norm_cross_correlation(image,template):
	#image = cv2.resize(image,(template.shape[1],template.shape[0]))
	w, h = template.shape[::-1]
	image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	if template.shape[0]>image.shape[0] or template.shape[1]>image.shape[1]:
		template = cv2.resize(template,(image.shape[1],image.shape[0]))
	res = cv2.matchTemplate(image,template,cv2.TM_CCORR_NORMED)
	#print res[0][0]
	min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
	top_left = max_loc
	bottom_right = (top_left[0] + w, top_left[1] + h)

	return res[0][0],top_left,bottom_right	

def NCC(templates,image):
	best_score = 0	
	for template in templates:
		try:
			match_score,_,_ = norm_cross_correlation(image,template)
			if best_score<match_score:
				best_score = match_score
		except:
			pass
	return best_score

def match_img_with_templ_using_orb_features(img1,img2):
	global orb
	# find the keypoints and descriptors with ORB
	kp1, des1 = orb.detectAndCompute(img1,None)
	kp2, des2 = orb.detectAndCompute(img2,None)
	# create BFMatcher object
	bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
	# Match descriptors.
	matches = bf.match(des1,des2)	
	# Apply ratio test
	good = 0
	total = 0
	for m,n in matches:
		total+=1
		if m.distance < 0.75*n.distance:
			good+=1
	if total!=0:				
		prob_of_good_match = good/total
		print(prob_of_good_match)
		return prob_of_good_match  
	else:
		print('no match found')
		return 0	

def YOLO_on_ROI(arg,_):
	image = arg[0]
	bbox = arg[1]
	w = bbox[2]-bbox[0]
	h = bbox[3]-bbox[1]
	x1 = int(bbox[0]-w/2)
	x2 = int(bbox[2]+w/2)
	y1 = int(bbox[1]-h/2)
	y2 = int(bbox[3]+h/2)
	# condition checking
	if y1<0: y1 = 0
	if x1<0: x1 = 0
	if y2>=image.shape[0]: y2 = image.shape[0]-1
	if x2>=image.shape[1]: x2 = image.shape[1]-1

	
	img_for_yolo = image[y1:y2,x1:x2]
	if img_for_yolo.shape[0]>0 and img_for_yolo.shape[1]>0:
		prob,pt1,pt2 = YOLO_v2(img_for_yolo)
		bbox_now = [pt1[0],pt1[1],pt2[0],pt2[1]]
		BBox_from_yolo = cropped_to_original_frame(bbox,bbox_now,image.shape)	
	return prob,BBox_from_yolo	

def multithreading(func,args,num_of_threads):
	begin_time = time.time()
	with ThreadPoolExecutor(max_workers = num_of_threads) as executor:
		res = executor.map(func,args,[begin_time for i in range(len(args))])
	return list(res)	

def multiprocessing(func,args,num_of_threads):
	begin_time = time.time()
	with ProcessPoolExecutor(max_workers = num_of_threads) as executor:
		res = executor.map(func,args,[begin_time for i in range(len(args))])
	return list(res)	 
