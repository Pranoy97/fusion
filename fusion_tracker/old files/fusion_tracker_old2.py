#!/usr/bin/env python
import sys
import cv2
import time
import numpy as np
import os.path

from helper_functions import variance_of_laplacian
from helper_functions import cropped_to_original_frame
from helper_functions import norm_cross_correlation
from helper_functions import YOLO_on_ROI
from helper_functions import multithreading
from helper_functions import multiprocessing

from constants_and_global_vars import threshold
from constants_and_global_vars import prob_thresh
from constants_and_global_vars import prob_thresh_for_templates
from constants_and_global_vars import variance_thresh	

sys.path.append('../')
sys.path.append('../yolo_python_wrapper')
from darknet_customized import YOLO_v2
from darknet_customized import YOLO_v2_multi	
from darknet_customized import YOLO_v2_spl_fun	
from tracker import re3_tracker

class fusionTracker(object):
	def __init__(self,starting_bbox,debug):
		self.tracker = re3_tracker.Re3Tracker()
		self.num_frames = 0
		self.fps_total = 0
		self.counter = 0
		self.bbox = []
		self.BBox_from_yolo = []
		self.start = True
		self.prob = 0
		self.img_for_yolo = np.zeros((640,480,3))
		self.templates = []
		self.max_prob = 0
		self.lost_track = False
		self.image = np.zeros((640,480,3))		
		self.starting_bbox = starting_bbox
		self.debug = debug
		self.initial_bboxes = {} # for multitracker method
		self.bboxes = []         # for multiTracker method
		self.counter_for_obj = 0 # for multiTracker method
		self.known = True
		self.start_yolo = True
		self.cls = ''

	def track(self,image):
		self.image = image
		begin_time = time.time()
		
		#tracking begins here,i.e start of tracking getting the bounding box from the user 
		if self.start:
			self.BBox_from_yolo = self.starting_bbox
			#initializing re3
			imageRGB = self.image[:,:,::-1]
			self.bbox = self.tracker.track('ardrone', imageRGB,self.starting_bbox)

			#check if YOLO knows this object or not
			w = self.starting_bbox[2]-self.starting_bbox[0]
			h = self.starting_bbox[3]-self.starting_bbox[1]
			x1 = int(self.starting_bbox[0]-1*w)
			x2 = int(self.starting_bbox[2]+1*w)
			y1 = int(self.starting_bbox[1]-1*h)
			y2 = int(self.starting_bbox[3]+1*h)	
			if y1<0: y1 = 0
			if x1<0: x1 = 0
			if y2>=self.image.shape[0]: y2 = self.image.shape[0]-1
			if x2>=self.image.shape[1]: x2 = self.image.shape[1]-1		
			self.img_for_yolo = self.image[y1:y2,x1:x2]
			self.prob,pt1,pt2,self.cls = YOLO_v2(self.img_for_yolo)		
			self.known = (not (self.prob==0))

			#updating counters
			self.counter+=1
			self.start = False

			#saving the template
			x1 = int(self.bbox[0])
			x2 = int(self.bbox[2])
			y1 = int(self.bbox[1])
			y2 = int(self.bbox[3])	
			template = cv2.cvtColor(self.image[y1:y2,x1:x2], cv2.COLOR_BGR2GRAY)
			self.templates.append(template)	

			if self.known: print('I am tracking a '+self.cls)
			else: print("I am tracking an object that I haven't seen before")

			return self.bbox		


		if self.known: 

			#print('2')
			if not self.lost_track:
				#print('3')
				if (self.counter>=threshold or (variance_of_laplacian(self.img_for_yolo)<variance_thresh)):
					#print 'hey'
					w = self.bbox[2]-self.bbox[0]
					h = self.bbox[3]-self.bbox[1]
					x1 = int(self.bbox[0]-1*w)
					x2 = int(self.bbox[2]+1*w)
					y1 = int(self.bbox[1]-1*h)
					y2 = int(self.bbox[3]+1*h)
					# condition checking
					if y1<0: y1 = 0
					if x1<0: x1 = 0
					if y2>=self.image.shape[0]: y2 = self.image.shape[0]-1
					if x2>=self.image.shape[1]: x2 = self.image.shape[1]-1

					
					self.img_for_yolo = self.image[y1:y2,x1:x2]
					#cv2.imshow('yolo',self.img_for_yolo)
					if self.img_for_yolo.shape[0]>0 and self.img_for_yolo.shape[1]>0:
						self.prob,pt1,pt2 = YOLO_v2_spl_fun(self.img_for_yolo,self.cls)
						bbox_now = [pt1[0],pt1[1],pt2[0],pt2[1]]
						self.BBox_from_yolo = cropped_to_original_frame(self.bbox,bbox_now,self.image.shape)

					# updating the re3 tracker
					if self.prob>prob_thresh and (self.img_for_yolo.shape[0]>0 and self.img_for_yolo.shape[1]>0) and self.counter==threshold: # if prob is good then update the bounding box 
						initial_bbox = self.BBox_from_yolo
						self.counter = 0
						imageRGB = self.image[:,:,::-1]
						self.bbox = self.tracker.track('ardrone', imageRGB)#,initial_bbox)
					else:
						self.lost_track = True
						self.counter = 0

				else:	
					#print('44')
					imageRGB = self.image[:,:,::-1]
					self.bbox = self.tracker.track('ardrone', imageRGB)	
					self.counter+=1					

			if self.lost_track:
				#print('5')
				# run yolo on the entire image as object not found in the expected ROI
				#print('operating on entire image')

				bboxes_from_yolo = YOLO_v2_multi(self.image,self.cls)

				#if len(bboxes_from_yolo)==0:print('object not found')
				#print(len(bboxes_from_yolo))
				list_for_keeping_track_of_bbox_match = []

				#if sufficient templates are present then follow the below paradigm to do a sort of low-end recognition of object
				if len(self.templates)>=10 and bboxes_from_yolo!=None:
					for item in bboxes_from_yolo:

						self.prob = item[0]
						if self.prob>prob_thresh:
							bbox_ = [item[1][0],item[1][1],item[2][0],item[2][1]]
							x1 = int(bbox_[0])
							x2 = int(bbox_[2])
							y1 = int(bbox_[1])
							y2 = int(bbox_[3])
							try:
								best_score,_,_ = norm_cross_correlation(self.image[y1:y2,x1:x2],self.templates[0])
							except:
								best_score = 0	
							for template in self.templates:
								try:
									match_score,_,_ = norm_cross_correlation(self.image[y1:y2,x1:x2],template)
									if best_score<match_score:
										best_score = match_score
								except:
									pass		
							list_for_keeping_track_of_bbox_match.append((best_score,bbox_))		

				# finding the bbox with the max prob of matching
				#print len(list_for_keeping_track_of_bbox_match)
				if len(list_for_keeping_track_of_bbox_match)>0:
					max_match_score = list_for_keeping_track_of_bbox_match[0][0]
					bbox_temp = list_for_keeping_track_of_bbox_match[0][1]
					for i in list_for_keeping_track_of_bbox_match:
						if max_match_score < i[0]:
							max_match_score = i[0]
							bbox_temp = i[1]
					#print(max_match_score)
					if max_match_score>0.85:	
						self.lost_track = False			
						self.counter = 0
						imageRGB = self.image[:,:,::-1]
						initial_bbox = bbox_temp
						self.bbox = self.tracker.track('ardrone', imageRGB,initial_bbox)					

					else:# let the re3 tracker keep tracking for now			
						imageRGB = self.image[:,:,::-1]
						try:
							self.bbox = self.tracker.track('ardrone', imageRGB)
						except:
							pass
				else:			
					imageRGB = self.image[:,:,::-1]
					try:
						self.bbox = self.tracker.track('ardrone', imageRGB)
					except:
						pass

		# object never seen by YOLO	
		else:
			imageRGB = self.image[:,:,::-1]
			self.bbox = self.tracker.track('ardrone', imageRGB)
			self.counter+=1																							

		# saving templates(i.e. first 30 bbox)
		if len(self.templates)<30 and self.prob>prob_thresh_for_templates:
			x1 = int(self.bbox[0])
			x2 = int(self.bbox[2])
			y1 = int(self.bbox[1])
			y2 = int(self.bbox[3])	
			template = cv2.cvtColor(self.image[y1:y2,x1:x2], cv2.COLOR_BGR2GRAY)
			self.templates.append(template)		
						
		end_time = time.time()
		self.num_frames+=1
		self.fps_total+= 1/(end_time-begin_time)
		avg_fps = self.fps_total/self.num_frames
		# if not self.debug:	
		print('Current fusion fps: %.3f FPS' % (1 / (end_time - begin_time)))
		print('average fusion fps: '+str(avg_fps)+' FPS')
		print('\n')
		return self.bbox

	def multi_track(self,image):
		self.image = image
		begin_time = time.time()

		#tracking begins here,i.e start of tracking getting the bounding box from the user 
		if self.start:
			print('multi tracking started')
			self.BBox_from_yolo = self.starting_bbox # here the starting_bbox is a list of lists(as multiple bbox are present)
			self.counter_for_obj = 0
			for item in self.BBox_from_yolo:
				self.counter_for_obj+=1
				bbox = [item[0],item[1],item[2],item[3]]
				self.initial_bboxes[self.counter_for_obj] = bbox
			#self.counter_for_obj = self.counter_for_obj_local	#updating the number of objects being tracked
			imageRGB = self.image[:,:,::-1]
			if self.counter_for_obj<2 and self.counter_for_obj>0:
				print("multiple objects not found")
				print('re-initializing multi tracking ....')
				self.start = True	
			elif self.counter_for_obj>=2:
				self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB,self.initial_bboxes) 	
				self.counter+=1
				self.start = False
			else:
				self.start = True # as object not found yet
				print("no objects found in the current frame at the begining")

		# if counter is greater than its threshold or if the image is blurry i.e. variance of laplacian is less than its thresh, then use YOLO(if possible) 
		elif self.counter>=threshold or (variance_of_laplacian(self.image)<variance_thresh):
			start = time.time()
			args = []
			for bbox in self.bboxes:
				args.append([self.image,bbox])										
			res = multithreading(YOLO_on_ROI, args, 1)

			print(time.time()-start)
			# updating the re3 tracker
			self.counter_for_obj = 0
			self.initial_bboxes = {}
			print('after multithreading')
			for item in res:
				self.counter_for_obj+=1
				prob = item[0]
				if prob>prob_thresh:
					self.initial_bboxes[self.counter_for_obj] = item[1]
			imageRGB = self.image[:,:,::-1]
			# following pythonic EAFP(Easier to ask for forgiveness than permission)	
			try:
				self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB,self.initial_bboxes)
			except:
				print('multiple objects not found')
				print('re-initializing multi tracking ....')
				self.start = True		
			if len(self.bboxes)>=2:
				self.counter = 0	

		# just tracking by re3			
		else:
			#if self.debug:
			print('running re3')
			imageRGB = self.image[:,:,::-1]
			self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB)
			self.counter+=1	
						
		end_time = time.time()
		self.num_frames+=1
		self.fps_total+= 1/(end_time-begin_time)
		avg_fps = self.fps_total/self.num_frames
		if not self.debug:	
			print('Current fusion fps: %.3f FPS' % (1 / (end_time - begin_time)))
			print('average fusion fps: '+str(avg_fps)+' FPS')
			print('\n')
		return self.bboxes

	def multi_track_without_multithreading(self,image):
		self.image = image
		begin_time = time.time()

		#tracking begins here,i.e start of tracking getting the bounding box from the user 
		if self.start:
			print('multi tracking started')
			self.BBox_from_yolo = self.starting_bbox # here the starting_bbox is a list of lists(as multiple bbox are present)
			self.counter_for_obj = 0
			for item in self.BBox_from_yolo:
				self.counter_for_obj+=1
				bbox = [item[0],item[1],item[2],item[3]]
				self.initial_bboxes[self.counter_for_obj] = bbox
			#self.counter_for_obj = self.counter_for_obj_local	#updating the number of objects being tracked
			imageRGB = self.image[:,:,::-1]
			if self.counter_for_obj<2 and self.counter_for_obj>0:
				print("multiple objects not found")
				print('re-initializing multi tracking ....')
				self.start = True	
			elif self.counter_for_obj>=2:
				self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB,self.initial_bboxes) 	
				self.counter+=1
				self.start = False
			else:
				self.start = True # as object not found yet
				print("no objects found in the current frame at the begining")

		# if counter is greater than its threshold or if the image is blurry i.e. variance of laplacian is less than its thresh, then use YOLO(if possible) 
		elif self.counter>=threshold or (variance_of_laplacian(self.image)<variance_thresh):
			print('yolo')
			start = time.time()
			bbox_list = []
			for bbox in self.bboxes:
				w = bbox[2]-bbox[0]
				h = bbox[3]-bbox[1]
				x1 = int(bbox[0]-w/2)
				x2 = int(bbox[2]+w/2)
				y1 = int(bbox[1]-h/2)
				y2 = int(bbox[3]+h/2)
				# condition checking
				if y1<0: y1 = 0
				if x1<0: x1 = 0
				if y2>=image.shape[0]: y2 = image.shape[0]-1
				if x2>=image.shape[1]: x2 = image.shape[1]-1
				
				img_for_yolo = image[y1:y2,x1:x2]
				if img_for_yolo.shape[0]>0 and img_for_yolo.shape[1]>0:
					prob,pt1,pt2 = YOLO_v2(img_for_yolo)
					bbox_now = [pt1[0],pt1[1],pt2[0],pt2[1]]
					BBox_from_yolo = cropped_to_original_frame(bbox,bbox_now,image.shape)
				bbox_list.append([prob,BBox_from_yolo])					


			print(time.time()-start)
			# updating the re3 tracker
			self.counter_for_obj = 0
			self.initial_bboxes = {}
			for item in bbox_list:
				self.counter_for_obj+=1
				prob = item[0]
				if prob>prob_thresh:
					self.initial_bboxes[self.counter_for_obj] = item[1]
			imageRGB = self.image[:,:,::-1]
			# following pythonic EAFP(Easier to ask for forgiveness than permission)	
			try:
				self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB,self.initial_bboxes)
			except:
				print('multiple objects not found')
				print('re-initializing multi tracking ....')
				self.start = True		
			if len(self.bboxes)>=2:
				self.counter = 0	

		# just tracking by re3			
		else:
			#if self.debug:
			print('running re3')
			imageRGB = self.image[:,:,::-1]
			self.bboxes = self.tracker.multi_track(range(1,self.counter_for_obj+1), imageRGB)
			self.counter+=1	
						
		end_time = time.time()
		self.num_frames+=1
		self.fps_total+= 1/(end_time-begin_time)
		avg_fps = self.fps_total/self.num_frames
		if not self.debug:	
			print('Current fusion fps: %.3f FPS' % (1 / (end_time - begin_time)))
			print('average fusion fps: '+str(avg_fps)+' FPS')
			print('\n')
		return self.bboxes		