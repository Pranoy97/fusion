#!/usr/bin/env python
import sys
import cv2
import time
import numpy as np
import os.path

basedir = os.path.dirname(__file__)
sys.path.insert(0, '/home/bingshen/object_track_ws/src/re3_tensorflow')
from tracker import re3_tracker
tracker = re3_tracker.Re3Tracker()
#importing files from "yolo_python_wrapper" folder
sys.path.insert(0, '/home/bingshen/object_track_ws/src/re3_tensorflow/yolo_python_wrapper')
from darknet_customized import YOLO_v2
from darknet_customized import YOLO_v2_multi

from constants_and_global_vars import *
from helper_functions import *

# callback function for fusing the tracker(re3) and detector(YOLO v2) algorithm
def main():
	begin_time = time.time()

	# importing global vars
	global num_frames
	global fps_total
	global start
	global counter
	global BBox_from_yolo
	global tracker
	global bbox
	global threshold
	global bbox_to_pub
	global prob
	global variance_thresh
	global out
	#global w,h
	global img_for_yolo
	global prob_thresh_for_templates
	global templates
	global max_prob
	global lost_track
	global image

	#tracking begins here,i.e start of tracking by detection using YOLO 
	if start:
		BBox_from_yolo = get_the_desired_roi(image)
		w = BBox_from_yolo[2]-BBox_from_yolo[0]
		h = BBox_from_yolo[3]-BBox_from_yolo[1]
		initial_bbox = BBox_from_yolo
		imageRGB = image[:,:,::-1]
		bbox = tracker.track('ardrone', imageRGB,initial_bbox)
		counter+=1
		start = False

	# if counter is greater thatn its threshold or if the image is blurry i.e. variance of laplacian is less than its thresh, then use YOLO(if possible) 
	elif counter>=threshold or (variance_of_laplacian(img_for_yolo)<variance_thresh):
		w = bbox[2]-bbox[0]
		h = bbox[3]-bbox[1]
		x1 = int(bbox[0]-w/4)
		x2 = int(bbox[2]+w/4)
		y1 = int(bbox[1]-h/4)
		y2 = int(bbox[3]+h/4)
		# condition checking
		if y1<0: y1 = 0
		if x1<0: x1 = 0
		if y2>=image.shape[0]: y2 = image.shape[0]-1
		if x2>=image.shape[1]: x2 = image.shape[1]-1

		
		img_for_yolo = image[y1:y2,x1:x2]
		if img_for_yolo.shape[0]>0 and img_for_yolo.shape[1]>0:
			prob,pt1,pt2 = YOLO_v2(img_for_yolo)
			bbox_now = [pt1[0],pt1[1],pt2[0],pt2[1]]
			BBox_from_yolo = cropped_to_original_frame(bbox,bbox_now,image.shape)					
			#cv2.imshow('cropped yolo',img_for_yolo)					

		#print prob

		# updating the re3 tracker
		if prob>prob_thresh and (img_for_yolo.shape[0]>0 and img_for_yolo.shape[1]>0) and counter==threshold and not lost_track: # if prob is good then update the bounding box 
			initial_bbox = BBox_from_yolo
			counter = 0
			imageRGB = image[:,:,::-1]
			bbox = tracker.track('ardrone', imageRGB,initial_bbox)
		else:
			# run yolo on the entire image as object not found in the expected ROI
			print('operating on entire image')
			lost_track = True
			bboxes_from_yolo = YOLO_v2_multi(image)
			#print len(bboxes_from_yolo)
			list_for_keeping_track_of_bbox_match = []

			#if sufficient templates are present then follow the below paradigm
			if len(templates)>=10:
				for item in bboxes_from_yolo:
					prob = item[0]
					if prob>prob_thresh:
						bbox_ = [item[1][0],item[1][1],item[2][0],item[2][1]]
						x1 = int(bbox_[0])
						x2 = int(bbox_[2])
						y1 = int(bbox_[1])
						y2 = int(bbox_[3])
						try:
							best_score = norm_cross_correlation(image[y1:y2,x1:x2],templates[0])
							#best_score = match_img_with_templ_using_orb_features(image[y1:y2,x1:x2],templates[0])
						except:
							best_score = 0	
						for template in templates:
							try:
								match_score = norm_cross_correlation(image[y1:y2,x1:x2],template)
								#match_score = match_img_with_templ_using_orb_features(image[y1:y2,x1:x2],template)
								if best_score<match_score:
									best_score = match_score
							except:
								pass		
						list_for_keeping_track_of_bbox_match.append((best_score,bbox_))		
			#print(len(list_for_keeping_track_of_bbox_match))				
			# finding the bbox with the max prob of matching
			if len(list_for_keeping_track_of_bbox_match)>0:
				print('finding bbox by using NCC')
				max_match_score = list_for_keeping_track_of_bbox_match[0][0]
				bbox_temp = list_for_keeping_track_of_bbox_match[0][1]
				for i in list_for_keeping_track_of_bbox_match:
					if max_match_score < i[0]:
						print i[0]
						max_match_score = i[0]
						bbox_temp = i[1]
				#print(max_match_score)	
				if max_match_score>0.8:	
					lost_track = False			
					counter = 0
					imageRGB = image[:,:,::-1]
					initial_bbox = bbox_temp
					bbox = tracker.track('ardrone', imageRGB,initial_bbox)					

			else:# let the re3 tracker keep tracking for now			
				imageRGB = image[:,:,::-1]
				try:
					bbox = tracker.track('ardrone', imageRGB)
				except:
					pass

	# just tracking by re3			
	else:
		imageRGB = image[:,:,::-1]
		bbox = tracker.track('ardrone', imageRGB)
		counter+=1	

	# saving templates
	if len(templates)<30 and prob>prob_thresh_for_templates:
		x1 = int(bbox[0])
		x2 = int(bbox[2])
		y1 = int(bbox[1])
		y2 = int(bbox[3])	
		template = cv2.cvtColor(image[y1:y2,x1:x2], cv2.COLOR_BGR2GRAY)
		templates.append(template)		
	elif len(templates)<60 and prob>max_prob:
		max_prob = prob
		x1 = int(bbox[0])
		x2 = int(bbox[2])
		y1 = int(bbox[1])
		y2 = int(bbox[3])	
		templates.append(image[y1:y2,x1:x2])
					
	end_time = time.time()
	num_frames+=1
	fps_total+= 1/(end_time-begin_time)
	avg_fps = fps_total/num_frames	
	print("average fusion fps: %s",str(avg_fps))

	try:
		#drawing the bounding box on the image and showing it
		#print prob
		cv2.rectangle(image,
		        (int(bbox[0]), int(bbox[1])),
		        (int(bbox[2]), int(bbox[3])),
		        [0,0,255], 2)
	except:
		print('object not found')	
	out.write(image)
	cv2.imshow('re3_YOLO_fusion', image)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		out.release()		

if __name__ == '__main__':
	global image
	path1 = '/media/lab/pranoy_usb/2.avi'
	path2 = '/media/bingshen/pranoy_usb/pranoy/output_rectified2_edited.mp4'
	path3 = '/media/bingshen/pranoy_usb/vid_people.mp4'
	path4 = '/media/bingshen/pranoy_usb/update fusion/vid_people_6.mp4'
	path5 = '/media/bingshen/pranoy_usb/ardrone_vid.mp4'
	path6 = '/media/bingshen/pranoy_usb/testing_data/person_crossing_road/video.avi'
	path7 = '/media/bingshen/pranoy_usb/testing_data/bolt_running/video.avi'
	path8 = '/media/bingshen/pranoy_usb/testing_data/car/video.avi'
	path9 = '/media/bingshen/pranoy_usb/testing_data/basketball/video.avi'
	path10 = '/media/bingshen/pranoy_usb/testing_data/cat/video.avi'
	path11 = '/home/bingshen/object_track_ws/src/test_data/iceskatter/video.avi'
	path12 = '/home/bingshen/object_track_ws/src/test_data/gymnastics/video.avi'
	path13 = '/home/bingshen/object_track_ws/src/test_data/girl_folder/video.avi'
	path14 = '/home/bingshen/object_track_ws/src/test_data/matrix/video.avi'
	path15 = '/home/bingshen/object_track_ws/src/test_data/pedestrian/video.avi'
	path16 = '/home/bingshen/object_track_ws/src/test_data/person/video.avi'
	path17 = '/home/bingshen/object_track_ws/src/test_data/racing/video.avi'
	path18 = '/home/bingshen/object_track_ws/src/test_data/singer/video.avi'
	path19 = '/home/bingshen/object_track_ws/src/test_data/motorcross/video.avi'
	path20 = '/home/bingshen/object_track_ws/src/test_data/book/video.avi'
	path21 = '/home/bingshen/object_track_ws/src/test_data/bicycle/video.avi'
	path22 = '/home/bingshen/object_track_ws/src/test_data/motorbike/video.avi'
	path23 = '/home/bingshen/object_track_ws/src/test_data/uav/video.avi'
	path24 = '/home/bingshen/object_track_ws/src/test_data/bird/video.avi'
	cam = cv2.VideoCapture(path22)
	ret = True
	begin = time.time()
	while time.time()-begin<100000:
		ret,image = cam.read()
		#image = cv2.resize(image,(640,480),interpolation = cv2.INTER_CUBIC)
		main()

