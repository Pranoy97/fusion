#!/usr/bin/env python
import sys
import cv2
import time
import numpy as np
import sys
import os.path
basedir = os.path.dirname(__file__)
sys.path.append('../')
sys.path.append('../yolo_python_wrapper')
from tracker import re3_tracker
import types
import numpy as np
from darknet_customized import YOLO_v2_multi

#global vars
num_frames = 0
fps_total = 0
counter = 0
bbox = []
BBox_from_yolo = []
tracker = re3_tracker.Re3Tracker()
threshold = 5
prob_thresh = 0.4
start = True
global prob
prob = 0
variance_thresh = 80
counter_for_obj = 0
initial_bboxes = {}

# for saving video
# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
#out = cv2.VideoWriter('/home/bingshen/object_track_ws/src/video.avi',fourcc, 30.0, (640,480))

#Helper function
def variance_of_laplacian(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	return cv2.Laplacian(image, cv2.CV_64F).var()

# callback function for fusing the tracker(re3) and detector(YOLO v2) algorithm

def main(image):
	begin_time = time.time()

	# importing global vars
	global num_frames
	global fps_total
	global start
	global counter
	global BBox_from_yolo
	global tracker
	global bbox
	global threshold
	global bbox_to_pub
	global prob
	global variance_thresh
	global out
	global counter_for_obj
	global initial_bboxes 

	# if counter is greater that its threshold or if the image is blurry i.e. variance of laplacian is less than its thresh, then use YOLO(if it det. with high prob) 
	if (counter>=threshold) or (variance_of_laplacian(image)<variance_thresh):		
		BBoxes_from_yolo = YOLO_v2_multi(image)
		counter_for_obj_local = 0
		for item in BBoxes_from_yolo:
			counter_for_obj_local+=1
			prob = item[0]
			if prob>prob_thresh:
				bbox = [item[1][0],item[1][1],item[2][0],item[2][1]]
				initial_bboxes[counter_for_obj_local] = bbox
			else:
				counter_for_obj_local-=1	# reduce the counter as YOLO is not confident of the detection	
		
		imageRGB = image[:,:,::-1]
		if counter_for_obj<2:# either 1 or no objects in the frame
			try:		
				bboxes,fps = tracker.track('ardrone', imageRGB,initial_bboxes[1])
				counter=0
			except:
				print "no objects found in the current frame "	
		elif counter_for_obj==counter_for_obj_local and counter_for_obj>=2:
			bboxes = tracker.multi_track(range(1,counter_for_obj+1), imageRGB,initial_bboxes)
			counter=0
		else: # if yolo cant find all the objects then use re3
			bboxes = tracker.multi_track(range(1,counter_for_obj+1), imageRGB,initial_bboxes)
				
				
	else:
		#tracking begins here,i.e start of tracking by detection using YOLO 
		if start:
			print 'tracking started'
			start = False
			BBoxes_from_yolo = YOLO_v2_multi(image)
			counter_for_obj_local = 0
			for item in BBoxes_from_yolo:
				counter_for_obj_local+=1
				prob = item[0]
				bbox = [item[1][0],item[1][1],item[2][0],item[2][1]]
				initial_bboxes[counter_for_obj_local] = bbox
			counter_for_obj = counter_for_obj_local	#updating the number of objects being tracked
			imageRGB = image[:,:,::-1]
			if counter_for_obj<2:
				try:
					bboxes,fps = tracker.track('ardrone', imageRGB,initial_bboxes[1])
					counter+=1
				except:
					print "no objects found in the current frame "	
			elif counter_for_obj>1:
				bboxes = tracker.multi_track(range(1,counter_for_obj+1), imageRGB,initial_bboxes) 	
				counter+=1
			else:
				start = True # as object not found yet
				print "no objects found in the current frame at the begining"

		else: # just tracking by re3				
			imageRGB = image[:,:,::-1]
			if counter_for_obj<2:# either 1 or no objects in the frame
				try:		
					bboxes,fps = tracker.track('ardrone', imageRGB)
				except:
					print "no objects found in the current frame "	
			else:
				bboxes = tracker.multi_track(range(1,counter_for_obj+1), imageRGB)
			counter+=1
	end_time = time.time()
	num_frames+=1
	fps_total+= 1/(end_time-begin_time)
	avg_fps = fps_total/num_frames	
	print "average fusion fps: %s",str(avg_fps)
	print 'currently ',str(counter_for_obj),' objects are being tracked'
	#print bboxes

	try:
		if True:
			for bbox in bboxes:
				cv2.rectangle(image,(int(bbox[0]), int(bbox[1])),(int(bbox[2]), int(bbox[3])),[0,0,255], 2)
			# Display tracker type on frame
			cv2.putText(image, "Fusion with blur check(Multi Tracker)", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50),2)

			# Display FPS on frame
			cv2.putText(image, "FPS : " + str(int(avg_fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)
			out.write(image)
		else:
			print type(bboxes[0])
			cv2.rectangle(image,(int(bboxes[0]), int(bboxes[1])),(int(bboxes[2]), int(bboxes[3])),[0,0,255], 2)	
			# Display tracker type on frame
			cv2.putText(image, "Fusion", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50),2)

			# Display FPS on frame
			cv2.putText(image, "FPS : " + str(int(avg_fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)
			out.write(image)
	except:
		#print 'Hmm'
		bboxes = [0,0,0,0]	

	cv2.imshow('fusion_multitrack', image)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		pass							

	return bboxes		

if __name__ == '__main__':
	path6 = '/home/bingshen/object_track_ws/src/test_data/person_crossing_road/video.avi'
	path7 = '/home/bingshen/object_track_ws/src/test_data/bolt_running/video.avi'
	path8 = '/home/bingshen/object_track_ws/src/test_data/car/video.avi'
	path9 = '/home/bingshen/object_track_ws/src/test_data/basketball/video.avi'
	path10 = '/home/bingshen/object_track_ws/src/test_data/cat/video.avi'
	path11 = '/home/bingshen/object_track_ws/src/test_data/iceskatter/video.avi'
	path12 = '/home/bingshen/object_track_ws/src/test_data/gymnastics/video.avi'
	path13 = '/home/bingshen/object_track_ws/src/test_data/girl_folder/video.avi'
	path14 = '/home/bingshen/object_track_ws/src/test_data/matrix/video.avi'
	path15 = '/home/bingshen/object_track_ws/src/test_data/pedestrian/video.avi'
	path16 = '/home/bingshen/object_track_ws/src/test_data/person/video.avi'
	path17 = '/home/bingshen/object_track_ws/src/test_data/racing/video.avi'
	path18 = '/home/bingshen/object_track_ws/src/test_data/singer/video.avi'
	path19 = '/home/bingshen/object_track_ws/src/test_data/motorcross/video.avi'
	path20 = '/home/bingshen/object_track_ws/src/test_data/book/video.avi'
	path21 = '/home/bingshen/object_track_ws/src/test_data/bicycle/video.avi'
	path22 = '/home/bingshen/object_track_ws/src/test_data/motorbike/video.avi'
	path23 = '/home/bingshen/object_track_ws/src/test_data/uav/video.avi'
	path24 = '/home/bingshen/object_track_ws/src/test_data/bird/video.avi'

	cam = cv2.VideoCapture(0)
	ret = True
	#file_object = open('Fusion_with_blur_check.txt','w')
	begin = time.time()
	while time.time()-begin<10000:
		ret,image = cam.read()
		#image = cv2.resize(image,(640,360),interpolation = cv2.INTER_CUBIC)
		bbox = main(image)
	#file_object.close()	

