# fusion

This repository is a work of Pranoy Panda and Prof. Martin Barczyk

fusion is a real-time object tracking algorithm. It offers accuracy and robustness similar to other state-of-the-art trackers while operating at 85-110 FPS. For more details, contact 1997pranoy@gmail.com. 

## Model:
This algorithm uses two pre-existing algorithm and fuses them in order to perform robust tracking of fast moving objects. The model weights we used in our paper can be downloaded from [here](https://goo.gl/NWGXGM), extract the tar, and place in your log directory (mine is "logs") and from [here](https://bitbucket.org/Pranoy97/fusion/src/master/yolo_python_wrapper/yolov2.weights){for yolov2 weights file}

## Requirements:
1. Python 2.7+ or 3.5+.
2. [Tensorflow](https://www.tensorflow.org/) and its requirements. I use the pip tensorflow-gpu==1.6.0
3. [NumPy](http://www.numpy.org/). The pip should work.
4. [OpenCV 2](http://opencv.org/opencv-2-4-8.html). The opencv-python pip should work.
5. [CUDA (Strongly Recommended)](https://developer.nvidia.com/cuda-downloads).
6. [cuDNN (Recommended)](https://developer.nvidia.com/cudnn).

## First Time Setup:
```bash
git clone 
cd fusion
sudo apt-get install python-virtualenv
virtualenv venv --system-site-packages
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```
### Enter the virtualenv in a later session to use the installed libraries.
```bash
source venv/bin/activate
```
### To exit the virtualenv
```bash
deactivate
```

## Folder and Files for the Fusion Algorithm
Todo 

## Folders and Files for the Object Detector(YOLO-v2)

## References(The work from these papers were directly used in our work)
```
@article{gordon2018re3,
  title={Re3: Real-Time Recurrent Regression Networks for Visual Tracking of Generic Objects},
  author={Gordon, Daniel and Farhadi, Ali and Fox, Dieter},
  journal={IEEE Robotics and Automation Letters},
  volume={3},
  number={2},
  pages={788--795},
  year={2018},
  publisher={IEEE}
}
```
```
@inproceedings{redmon2017yolo9000,
  title={YOLO9000: better, faster, stronger},
  author={Redmon, Joseph and Farhadi, Ali},
  booktitle={Proceedings of the IEEE conference on computer vision and pattern recognition},
  pages={7263--7271},
  year={2017}
}
```
